package main

import (
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"

	_ "github.com/joho/godotenv/autoload"
)

const (
	upstreamBaseURLEnv = "UPSTREAM_BASE_URL"
	listenAddrEnv      = "LISTEN_ADDRESS"
)

func main() {
	fmt.Println("starting server")

	upstreamBaseURL := os.Getenv(upstreamBaseURLEnv)
	fmt.Printf("%s=%s\n", upstreamBaseURLEnv, upstreamBaseURL)

	listenAddr := os.Getenv(listenAddrEnv)
	if listenAddr == "" {
		listenAddr = "0.0.0.0:8080"
	}
	fmt.Printf("%s=%s\n", listenAddrEnv, listenAddr)

	if err := http.ListenAndServe(listenAddr, router{
		client:  http.DefaultClient,
		baseUrl: upstreamBaseURL,
	}); err != nil {
		fmt.Printf("ERROR: %s\n", err.Error())
		os.Exit(1)
	} else {
		os.Exit(0)
	}
}

type router struct {
	client  *http.Client
	baseUrl string
}

func (h router) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	urlString := r.URL.String()
	header := r.Header

	fmt.Println(urlString)
	newUrl, err := url.Parse(h.baseUrl + "/" + urlString)
	if err != nil {
		internalServerError(w, err)
		return
	}

	resp, err := h.client.Do(&http.Request{
		Method: r.Method,
		Body:   r.Body,
		Header: header,
		URL:    newUrl,
	})
	if err != nil {
		internalServerError(w, err)
		return
	}

	copyHeader(w.Header(), resp.Header)

	// change delete specific header key-value pair here
	// e.g. w.Header().Del("X")

	w.WriteHeader(resp.StatusCode)
	if _, err := io.Copy(w, resp.Body); err != nil {
		internalServerError(w, err)
	}
	return
}

func copyHeader(dst, src http.Header) {
	for k, vv := range src {
		for _, v := range vv {
			dst.Add(k, v)
		}
	}
}

func internalServerError(w http.ResponseWriter, err error) {
	w.WriteHeader(http.StatusInternalServerError)
	w.Write([]byte(err.Error()))
	return
}
